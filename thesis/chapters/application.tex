\chapter{Application} \label{ch:application}
This chapter describes the concepts used in the library and proof of concept game.

\section{General} \label{sec:app:general}
In this thesis, three kinds of levels can be generated: \textbf{Landscape}, \textbf{cave} and \textbf{dungeon}. On every level, missions are generated, which take a role in shaping the level to ensure that all points of interest are reachable.\\
The \textbf{landscape} serves as a hub-level and consists of a basic height map and connects various other points of interest. The landscape will be generated once and then stored to not confuse the player when he searches for a certain location. A key aspect is the transition to a cave and dungeon, which are placed randomly and lead to newly generated levels every time, but can only be accessed once per play session.\\
A \textbf{cave} is a type of sub level and is generate from Perlin noise, which was used because it is suitable to achieve the natural look required.\\
\textbf{Dungeons} on the other hand should look like they have been built by humans, so the more natural and organic look resulting from using fractals is out of the question. A approach based on a graph structure is used instead.

\subsection{Missions} \label{sec:app:missions}
The missions are the tasks a player has to do in order to complete the game and are the basis for generating terrain sketches, which will be explained in detail in section \ref{sec:dev:landspace}.\\
Missions are organized in an acyclic graph structure and consists of objectives, which are represented as the nodes. Some objectives have to be completed in a specific order, as they can have prerequisites, for example a has to be collected before chest can be opened. Others can be completed in any order, which is also represented by edges in the graph.\\
Every graph consists of a start and end node, which are introduced to make it possible to have multiple missions in one graph, although it is currently not used. The objectives vary depending on the scene they are generated for. The process of generating a mission is loosely based on Lindenmayer systems~\cite{lindenmayer1968mathematical}, although a lot simpler. A simple grammar is defined and the number of productions is based on the selected difficulty.\\
%TODO Erklärung noise texture
The nodes are placed on a \texttt{Texture2D} of the same size as the noise texture, the nodes are placed in segments, which are calculated through the size of the texture and the number of objective types in the graph, with start and end nodes also being a type. This results in a random distribution, but makes sure to use most of the available space, although they can be clustered. This can be solved by using a more advanced distribution, as projected in chapter \ref{ch:limitations-futurework}.\\
There are different sets of possible nodes, depending on the level they are generated for. Landscape missions are meant to lead the player into caves or dungeons, which then have their own missions. They are only generated once upon the start of a game, whereas the missions in caves and dungeons are generated every time the player enters it. A player can chose to abandon the mission by returning to the starting point or use the normal exit, but only after all objectives are completed. The mission on the landscape will be marked as fulfilled.

\subsection{Landscape} \label{sec:app:pcg-landscape}
Generating landscapes has been a very well researched topic and for this task, plasma fractals generated via \gls{perlinnoise} are used. The approach used here is based heavily on one explained by a user called "scrawk" in his blog ~\cite{unity-simple-procedural-terrain}. It uses the concept of \gls{25d-terrain}, which means that for every point in a field of X and Z-coordinates, there is exactly one Y-value defining the height of the point. While this does not allow for certain landscape features like overhangs, it is a simple and effective method.\\
One \gls{heightmap} is generated from two components: the plain- and mountain noise. Both have the same dimensions, the main difference is in the amplitude. Plain noise only has a very narrow amplitude, resulting in gentle terrain features. To generate distinct terrain features, the mountain noise uses an amplitude several times that of the plain noise, resulting in high spikes of values. As they are both converted into a \texttt{Texture2D} object, the negative values are all cut off at zero, resulting in completely even terrain at those spots. Both noises are added together, resulting in high amplitude spikes for mountains where the plain noise does not have a significant impact, while on the completely even places it generates a gently surface, resulting in a more realistic look.\\
The landscape makes use of the Terrain data structure provided by Unity, which has built-in features for tiles of geometry, splatmaps for texturing and more. Unfortunately, it seems that it is intended to be mainly used through the UI, as the documentation is rather lackluster.\\
Terrain can be split in tiles, defined by the neighbors property, which automatically smooths adjacent edges. This features was used in the early stages of development, but was deactivated with the introduction of terrain sketches. While not impossible, implementation of terrain sketches spread over multiple tiles was postponed due to time constraints.\\
As the terrain is generated at runtime, texturing is a problem. To solve this, texture splatting~\cite{bloom2000texturesplatting} can be used to dynamically apply texture onto the geometry, depending on parameters. Texture splatting uses splatmaps, which are essentially alphamaps the size of the \gls{heightmap}, defining the weight of a texture in the combined overall texture. In Unity, the two obvious parameters are height and steepness of a point. Depending on these two parameters, the mixture of textures can be calculated. In this case, textures for lower regions are grass texture, higher textures become more dirt and rock heavy, while at the top a snow texture is used. The steepness parameter is currently not used.\\
The terrain class has a number of other features, including placement of trees and other small objects, which are currently not used.
\\ 
A major problem when generating landscapes for games is the random nature of the process, which makes it possible to have points of interest in unreachable spots. To solve this problem, the concept of terrain sketches was adapted to the purpose.\\
In the approach by Zhou et al.~\cite{zhou2007terrain}, the concept was used to generated notable features of the landscape, in this case mountain ridges. First, an existing \gls{heightmap} was analyzed on the characteristic features, which were extracted as small tiles. Those tiles were then applied to a simple graphic, containing a sketch drawn by a human, while the rest of the \gls{heightmap} was filled with other features found in the existing \gls{heightmap} and smoothed out.\\
This process was adapted to use the generated plain- and mountains noise described earlier. The sketch was generated based on the mission graph described in section \ref{sec:dev:missions}, with the points of interest placed randomly in segments of the map based on the number of missions and to make use of the complete map. They are then connected as defined in the mission graph, which results in the sketch.\\
Instead of just adding the two noisemaps together, the concept is to weight the plains noise higher the closer it is to the sketch, and vice versa for the mountains noise. The result ensures that points of interest are reachable for the player, the look depends on the other noise, especially the mountain noise. It is possible to cut through a mountain region, which should be smooth.

\begin{figure}[htbp]
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{images/Landscape_sketch.png}
        \caption{landscape sketch}
        \label{fig:landscape-sketch}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{images/noisetexture_terrain_weights.png}
        \caption{terrain weights map}
        \label{fig:terrain-weights-map}
    \end{subfigure}
    \caption{sketches for terrain}\label{fig:terrain-sketch}
\end{figure}

\begin{figure}[htbp]
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{images/noisetexture_terrain_plain.png}
        \caption{landscape plains noise}
        \label{fig:landscape-plainsnoise}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{images/noisetexture_terrain_mountains.png}
        \caption{landscape mountain noise}
        \label{fig:landscape-mountain-noise}
    \end{subfigure}
    \caption{landscape terrain noise}\label{fig:landscape-terrain-noise}
\end{figure}

\begin{figure}[htbp]
    \centering
    \includegraphics[width=0.49\textwidth]{images/noisetexture_terrain_complete.png}
    \caption{complete terrain \gls{heightmap}}
    \label{fig:terrain-heightmap-complete}
\end{figure}

\subsection{Cave} \label{sec:app:caves}
Caves also use \gls{perlinnoise}, but only one layer and the further processing is different: the noise is evaluated and if the values exceed a set or calculated threshold, they are set to the maximum value and vice versa. This results in a black and white noisemap with natural looking structures, the details depend on the settings used when generating the noise. A minor flaw is that all walls seem angled, as the noisemap specifies a \gls{25d-terrain} \gls{heightmap} and as a result, multiple Z-values for every x/y-coordinate are not possible.\\
The noisemap is then used to generate a mesh, where the color values are used to determine the z-coordinates of a vertex. Dynamic texturing similar to splatmaps in the landscape was not implemented due to time constraints and because graphical fidelity was not a high priority. The mesh generator used was taken from the Procedural Examples package~\cite{unity-proceduralexamples} published in the Unity Asset Store.\\
Terrain sketches are used similar as in terrain generation, although no weighting is used as it is only one texture. They are simply drawn onto the existing texture with a set width to create accessible paths.

\begin{figure}[ht]
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{images/noisetexture_aftergeneration_perlinbasic.png}
        \caption{raw noise for caves}
        \label{fig:cave-noise-raw}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{images/noisetexture_aftercave_withoutsketch.png}
        \caption{noise after cave}
        \label{fig:cave-noise-aftercave}
    \end{subfigure}
    \caption{cave generation from noise}\label{fig:cave-noise}
\end{figure}


\begin{figure}[ht]
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{images/Cave_sketch.png}
        \caption{cave sketch}
        \label{fig:cave-sketch-raw}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.49\textwidth}
        \includegraphics[width=\textwidth]{images/noisetexture_aftercavemap.png}
        \caption{cave with sketch}
        \label{fig:cave-after-sketch}
    \end{subfigure}
    \caption{cave with sketch}\label{fig:cave-sketch}
\end{figure}

\subsection{Dungeon} \label{sec:app:pcg-dungeons}
The dungeon generation uses a completely different concept than the previous two methods. The commercial game TinyKeep~\cite{game-tinykeep} uses an approach based on nodes and triangulation, as explained by the developer Phi Dinh~\cite{dungeon-generation-tinykeep}. Based on that, a similar but simpler approach was developed.\\
The basic premise is still the same: rooms are generated and used as nodes in a graph, which represents how the rooms are connected. Every room is a rectangle, described through its lower left and upper right point. Additionally, the center point is calculated and stored as it is used for graph calculation. With this approach, the usage of a terrain sketch as used in the other level types is not necessary, as the graph structure already ensures that all points of interest are reachable.\\
For the graph, numerous types are looked at: a minimum spanning tree is a well understood graph, but it generates a lot of dead ends, which is not desired in this case. The favorites were a Gabriel graph~\cite{wiki-gabriel-graph} and relative neighborhood graph~\cite{toussaint1980relative}. They both generate structures that resemble human-made structures. The relative neighborhood graph was ultimately chosen as it is a lot less complex to implement, although the naive implementation is of $O(n^3)$ complexity. But since the current maximum number of rooms is only set to 10, it is not of concern, but should be noted in the future if larger structures are to be generated. An algorithm with $O(n^2)$ complexity using a Voronoi diagram and Delaunay triangulation is described in the paper.\\
The relative neighborhood graph was developed to extract a perceptually meaningful structure from a set of points, similar to how a human would group points. In this type of graph, two nodes are connected if there are no other nodes that are closer to either of the two than they are to each other.\\
At the end of the generation process, the rooms are drawn onto a texture to use the same mesh generator used in cave generation to create the actual geometry. It suffers from the same downside of using a \gls{25d-terrain} and seemingly angular walls.

\begin{figure}[ht]
    \begin{subfigure}[b]{0.24\textwidth}
        \includegraphics[width=\textwidth,frame]{images/2015-3-31_13-50-33.png}
%        \caption{dungeon example 1}
        \label{fig:subfig1}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.24\textwidth}
        \includegraphics[width=\textwidth,frame]{images/2015-3-31_13-52-37.png}
 %       \caption{dungeon example 2}
        \label{fig:subfig2}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.24\textwidth}
        \includegraphics[width=\textwidth,frame]{images/2015-4-1_17-17-43.png}
  %      \caption{dungeon example 3}
        \label{fig:subfig3}
    \end{subfigure}
    \hfill
    \begin{subfigure}[b]{0.24\textwidth}
        \includegraphics[width=\textwidth,frame]{images/2015-4-1_17-23-44.png}
   %     \caption{dungeon example 4}
        \label{fig:subfig4}
    \end{subfigure}
    \caption{Examples for generated dungeons}\label{fig:subfigs}
\end{figure}

\section{Adaptivity} \label{sec:app:adaptivity}
Difficulty settings are implemented and affect the number of missions and objectives generated. They have to be set manually, as they are currently only implemented with future developments in mind. They can be changed during the game, but only affect it before a level is generated. Missions in the landscape are only generated at the start of the game and are not affected by changes during the game.

\section{Game Concept}
The game follows a rather simple concept, as the focus of this thesis is the procedural generation in general, the game is only used as a proof of concept. Initially, a larger concept involving a small role-playing game was planned, but not implemented due to time constraints.\\
The starting point of the game is the landscape, were markers are placed to transition to caves and/or dungeons, which are randomly selected and placed. Inside a dungeon or cave, a submission is generated and in its current state, points of interest are generated according to the missions and placed on the map randomly on the map and are shown by markers. To have a minimal amount of actual gameplay, players are required to run into a marker to fulfill the objective.