Aufbau: R�ume, per Gang mit einandern verbunden. 
Abstraktion zu Graphtheoretischen Problem: R�ume entsprechen Knoten, G�nge Kanten.
Statt Raumausdehnung wird der Mittelpunkt als Knoten gew�hlt. Passender Graph w�hlen zur Verbindung um ein glaubw�rdiges Aussehen und Struktur zu erzeugen. G�nge sollte sich nicht �berm��ig �berschneiden, wenn dann "nat�rlich".

Erzeugen der R�ume als Abstrakte Form aus Punkte links unten und rechts oben, definieren ein Rechteck. Nachbarn sind R�ume, welche �ber Kanten im Graph mit aktuellem Verbunden sind. F�r das Erzeugen werden die Koordinaten auf eine Noisemap �bertragen und mit dem Meshgenerator ein Mesh erzeugen.

Graphen:
Minimum Spanning Tree: einfach, erzeugt viele Sackgassen und unglaubw�rdige Struktur
Gabriel Graph: erscheint passend, aber sehr komplex in der Erzeugung
Relative Neighbourhood Graph: erzeugt passenden Graph, kann Sackgassen enthalten aber Prinzip bedingt eher wenige. Kann einfach, aber langsam (O(n^3)) erzeugt werden oder effizient aus Voronoi-Diagramm und Delaunay Triangulation (O(n^2)) oder noch besser -> sp�ter, bei der aktuellen Anzahl von max. 10 R�umen/Knoten ist die Laufzeit noch kein gro�es Problem. Die Struktur sollte �berm��ige �berschneidungen von vornherein ausschlie�en. Es kann welche geben, z.b. T-Kreuzungen, diese werden aber als Glaubw�rdig empfunden. G�nge �ber direkte Distanz und daraus ein rechtwinkliges Dreieck bilden zeichnen. Anordnung der R�ume erfolgt halb-systematisch: erste Position ist fest, die daraus folgenden relativ dazu, Richtung und Gr��e zuf�llig -> erzeugt eine kompakte Struktur (weniger und k�rzere G�nge als komplett zuf�llig)
