- Cave-Generator: 3 Stufen (hoch, mittel tief), gestaltet das ganze etwas abwechlsungsreichner:
	- mittlere stufe wird Spiele level, höhe Wände und untere Wasser/Lava, aber auch ein hindernis. was zu was wird wird ausgewürfelt
- Außenwelt: Inselgenerator, auch für neue, damit Welt keinen Immersionsbruch erleidet wenn neue Teile generiert werden.
	- alternativ: keine fließenden Levelgrenzen, dann sollte es auch gehen. -> andere, "natürliche" grenze
	- Umgebungstextur wird ausgewürfelt -> splatmap
	- wie Wasser in Unity erstellen? -> gameobject, kann manipuliert werden, darstellung über shader, gibts im standard asset paket

- Kampfsystem:
	- bei Update() und laufen (evtl. als flag) würfeln ob random encounter
	- extra szene: erst statisch testen und dann gegner genieren: anzahl+welche (je nach Umgebung können anderen erscheinen)
	
	
Other
- Welche Perlin Implementierung? -> am besten auf eine beschränken um Verwirrung zu vermeiden
